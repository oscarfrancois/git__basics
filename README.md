# Cette activité vous permet de pratiquer quelques commandes de base git.

Git est le standard de l'industrie pour la gestion de version logicielle.

Les commandes suivantes sont à exécuter dans un environnement en ligne de 
commande (shell sous mac/linux, git bash sous windows).

* Créez puis modifiez un fichier texte au nom et contenu de votre choix 
dans un nouveau répertoire.

* Initialisez votre dépôt git dans ce répertoire via la commande:
```
cd MON_NOUVEAU_REPERTOIRE
git init
```

* Description du processus git

![git__process](git__process.png)

* Constatez l'état actuel des fichiers dans le processus git avec la commande
```
git status
```

Prenez note des éléments sous la mention: "untracked files"

* Ajoutez votre fichier à la zone de pré-commit ("staging area"):
```
git add MON_FICHIER
```

* Constatez le changement de zone de votre fichier via la commande:
```
git status
```

* Modifiez maintenant votre fichier (modifiez légèrement sont contenu).
Vous pouvez ensuite afficher les changements par rapport à la staging area via:
```
git diff
```

En blanc, la portion de texte qui était déjà en "staging area" et 
qui n'a pas changé.
En vert précédé des +, la nouvelle portion de texte qui n'est pas 
encore ajoutée à la staging area.

* Ajoutez vos derniers changements à la staging area via la commande:
```
git add MON_FICHIER
```

* Soumettez vos changements en local (commit local):
```
git commit -m 'mon message de commit'
```

Remarques concernant le message de commit:
- il doit être entouré de cotes.
- Il doit être court et écrit au présent.

* Votre changement fait maintenant parti du dépôt et est consultable dans 
l'historique des changements via la commande:
```
git log
```

Remarque: pour chaque changement (commit), les champs affichés sont:
- une somme de contrôle (SHA) constituant un identifiant unique
- l'auteur (avec nom, prénom et courriel)
- le message du changement (commit)


# Bilan des commandes vues ensemble:

git init: création d'un nouveau dépôt git

git status: inspecte le répertoire de travail git 
("working directory") et la zone de pré-commit ("staging area").

git add: ajoute les fichiers indiqués du répertoire de travail vers la 
zone de pré-commit ("staging area").

git diff: affiche la différence entre la zone de travail ("working area") et
le zone de pré-commit ("staging area").

git commit: déplace le changement de la zone de pré-commit ("staging area") 
vers le dépôt ("repository"). Il y sera dorénavant stocké de manière permanente.

git log: affiche la liste de tous les précédents changements ("commits").
